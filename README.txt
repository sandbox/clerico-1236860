
**Description

This module allows to bind Drupal with a Symfony application.


**Benefits

This module allows to get best of both worlds, CMS and frameworks.


**Installation AND Upgrades:

See the INSTALL.txt file.


** Symfony pages

A Symfony page delegates whole page construction to a Symfony action.

Go to admin/build/symfony, then click 'Add a Symfony page'
Define a title (Drupal's page title)
Give a unique key
Set the Uri of your page. This Uri should be defined by a route in your Symfony application (probably in your routing.yml config file).
So this Uri defines a module/action pair in your Symfony application.

Example: 
Title: Hello World !
Key: hello-world
Uri: hello

** Symfony blocks

A Symfony block is a standard Drupal block but add a parameter: a slot name.

Go to admin/build/block, then click 'Add a Symfony block'
So you can define a slot name in order to be mapped with a slot in Symfony.


** Filter

Symfony module defines a Drupal filter.
You can enable it, go to admin/settings/filters
(For instance, in the Full HTML input format).

Then, when you write HTML in textarea, you can enter placeholders like:
{symfony://module/component}

This 'Url' identifies a Symfony component in a module of your bound application.

You can add parameters to this Url.
Example: {symfony://mymodule/mycomponent1?name=foo}


**Symfony side

In the previous example, let's consider you have a Symfony application "frontend", with

routing.yml:

hello:
  url: /hello
  params: {module: mymodule, action:hello}

actions.class.php in mymodule/actions folder:

class mymoduleActions extends sfActions {

  function executeHello(sfRequest $request) {
    // Nothing to do
  }
}

components.class.php in mymodule/components folder:

class mymoduleComponents extends sfComponents {

  function executeMycomponent1(sfRequest $request) {
    // Nothing to do
  }
}

In the mymodule/templates folder:
helloSuccess.php:

<?php slot('left') ?>
Symfony left slot for action hello
<?php end_slot(); ?>

<h1>Hello World !!!!!</h1>

_mycomponent1.php

Name=<?= $name ?>


So in this example, if you go to "/hello" in your Drupal site, the hello action in your Symfony module mymodule will be executed.

In you place a Symfony block, with a slot name to "left", its content will be: "Symfony left slot for action hello"

The placeholder {symfony://mymodule/mycomponent1?name=foo} will be replaced by "Name=foo"