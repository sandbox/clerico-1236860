<?php
/*
 * This file is part of the Drupal Symfony module.
 * (c) 2010 J�r�me CLERICO <jerome.clerico@indigen.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


/**
 * sfDrupalPlugin configuration.
 * 
 * @package     sfSympagePlugin
 * @subpackage  config
 * @author      Your name here
 * @version     SVN: $Id: PluginConfiguration.class.php 17207 2009-04-10 15:36:26Z Kris.Wallsmith $
 */
class sfDrupalPluginConfiguration extends sfPluginConfiguration {

  const VERSION = '1.0.0-DEV';

  /**
   * @see sfPluginConfiguration
   */
  public function initialize() {
    if ( sfConfig::get('sf_enabled_modules') )
      sfConfig::set('sf_enabled_modules', array_merge(sfConfig::get('sf_enabled_modules'), array('DrupalMain')));
    else
      sfConfig::set('sf_enabled_modules', array('DrupalMain'));

    // TODO config.
  }
}
