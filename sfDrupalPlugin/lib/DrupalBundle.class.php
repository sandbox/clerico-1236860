<?php
/*
 * This file is part of the Drupal Symfony module.
 * (c) 2010 J�r�me CLERICO <jerome.clerico@indigen.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

 /**
  *
  */
class DrupalBundle  {

  protected static $_instance = null;

  public static function getInstance() {
    if ( self::$_instance === null )
      self::$_instance = new DrupalBundle();
    return self::$_instance;
  }

  protected $_breadcrumb = null;
  protected $_title = null;

  protected function DrupalBundle() {
    // Nothing to do.
  }

  /* Methods. */

  /**
   *
   */
  public function addToBreadcrumb($label, $url) {
    if ( $this->_breadcrumb === null )
      $this->_breadcrumb = array();
    $this->_breadcrumb[] = array('label' => $label,
                                 'url' => $url);
  }

  /**
   *
   */
  public function setBreadcrumb($breadcrumb = null) {
    if ( $breadcrumb === null )
      $breadcrumb = $this->_breadcrumb;
      $_breadcrumb = menu_get_active_breadcrumb();
    if ( $breadcrumb ) {
      foreach ( $breadcrumb as $part )
        $_breadcrumb[] = '<a href="' . $part['url'] . '">' . $part['label'] . '</a>';
    }
      drupal_set_breadcrumb($_breadcrumb);
    }

  /**
   *
   */
  public function setTitle($title = null) {
    if ( $this->_title !== null )
      return;
    if ( ($title === null) && ($this->_breadcrumb !== null) ) {
      if ( isset($this->_breadcrumb[count($this->_breadcrumb) - 1]['label']) )
        $title = $this->_breadcrumb[count($this->_breadcrumb) - 1]['label'];
    }
    if ( $title !== null )
      drupal_set_title($title);
    $this->_title = $title;
  }

  /**
   *
   */
  public function getFilesBasePath() {
    return file_directory_path();
  }

  /**
   *
   */
  public function getFilesBaseUrl() {
    return url(file_directory_path());
  }

  /**
   *
   */
  public function getPublicPath($path, $absolute = false) {
    // TODO: absolute with Drupal ?
    //return 'http://www2.eurecom.fr/' .  ltrim($path, '/');
    return '/' . trim(path_to_theme(), '/') . '/' . ltrim($path, '/');
  }
 
}
