<?php
/*
 * This file is part of the Drupal Symfony module.
 * (c) 2010 J�r�me CLERICO <jerome.clerico@indigen.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 */
class DrupalContext extends sfContext {
  
  static public function createInstance(sfApplicationConfiguration $configuration, $name = null, $class = __CLASS__) {
    // To ensure to create a "DrupalContext" instance.
    return sfContext::createInstance($configuration, $name, $class);
  }
  
  public function initialize(sfApplicationConfiguration $configuration) {

    $this->configuration = $configuration;
    $this->dispatcher    = $configuration->getEventDispatcher();

    try
    {
      $this->loadFactories();
    }
    catch (sfException $e)
    {
      $e->printStackTrace();
    }
    catch (Exception $e)
    {
      sfException::createFromException($e)->printStackTrace();
    }

    $this->dispatcher->connect('template.filter_parameters', array($this, 'filterTemplateParameters'));

    // DO NOT register our shutdown function
    //register_shutdown_function(array($this, 'shutdown'));
  }
}