<?php
/*
 * This file is part of the Drupal Symfony module.
 * (c) 2010 J�r�me CLERICO <jerome.clerico@indigen.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 */
class DrupalRenderingFilter extends sfRenderingFilter {

  /**
   * Executes this filter.
   *
   * @param sfFilterChain $filterChain The filter chain.
   *
   * @throws <b>sfInitializeException</b> If an error occurs during view initialization
   * @throws <b>sfViewException</b>       If an error occurs while executing the view
   */
  public function execute($filterChain) {
    // execute next filter
    $filterChain->execute();

    // get response object
    $response = $this->context->getResponse();

    // hack to rethrow sfForm and|or sfFormField __toString() exceptions (see sfForm and sfFormField)
    if (sfForm::hasToStringException())
    {
      throw sfForm::getToStringException();
    }
    else if (sfFormField::hasToStringException())
    {
      throw sfFormField::getToStringException();
    }

    // Add breadcrumb.
    DrupalBundle::getInstance()->setBreadcrumb();

    // Add title.
    DrupalBundle::getInstance()->setTitle();    

    // send headers + content
    //$response->send();
    // TODO.
  }
}
