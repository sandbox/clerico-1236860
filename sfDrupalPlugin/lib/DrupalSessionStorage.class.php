<?php
/*
 * This file is part of the Drupal Symfony module.
 * (c) 2010 J�r�me CLERICO <jerome.clerico@indigen.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

 /**
  *
  */
class DrupalSessionStorage extends sfSessionStorage {
  
  /**
   *
   */
  public function initialize($options = null) {
    // Bypass sfSessionStorage::initialize() 
    sfStorage::initialize($options);
    self::$sessionStarted = true;
  }
  
  /**
   *
   */
  public function regenerate($destroy = false) {
    if (self::$sessionIdRegenerated)
      return;
    
    // TODO
    // regenerate a new session id once per object
    //session_regenerate_id($destroy);

    self::$sessionIdRegenerated = true;
  }
}
