<?php
/*
 * This file is part of the Drupal Symfony module.
 * (c) 2010 J�r�me CLERICO <jerome.clerico@indigen.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 */
class DrupalWebRequest extends sfWebRequest {

  /**
   *
   */
  public function initialize(sfEventDispatcher $dispatcher, $parameters = array(), $attributes = array(), $options = array()) {
    if ( !isset($options['relative_url_root']) )
      $options['relative_url_root'] = DrupalBundle::getInstance()->getPublicPath('/');
    parent::initialize($dispatcher, $parameters, $attributes, $options);
  }

  /**
   *
   */
  public function getPathInfo() {
    return parent::getPathInfo();
  }

  /**
   *
   */
  public function getPathInfoPrefix() {
    $baseUrl = variable_get('symfony_baseurl', '');
    return ( ( $baseUrl ) ? $baseUrl : rtrim(base_path(), '/') );
  }

  /**
   *
   */
  public function getMethod() {
    return parent::getMethod();
  }

  /**
   *
   */
  public function getRequestFormat() {
    return parent::getRequestFormat();
  }

  /**
   *
   */
  public function getHost() {
    return parent::getHost();
  }

  /**
   *
   */
  public function isSecure() {
    return parent::isSecure();
  }

  /**
   *
   */
  public function getUri() {
    return parent::getUri();
  }
}
