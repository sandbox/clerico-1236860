<?php
/**
 * This file is part of the Drupal Symfony module.
 * (c) 2010 J�r�me CLERICO <jerome.clerico@indigen.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 */
function drupal_public_path($path, $absolute = false) {
  return DrupalBundle::getPublicPath($path, $absolute);
}

/**
 *
 */
function drupal_format_date($date, $format = 'd', $culture = null, $charset = null) {
  static $dateFormats = array();

  if (null === $date)
    return null;

  if (!$culture)
    $culture = sfContext::getInstance()->getUser()->getCulture();

  if (!$charset)
    $charset = sfConfig::get('sf_charset');

  if (!isset($dateFormats[$culture]))
    $dateFormats[$culture] = new sfDateFormat($culture);

  return $dateFormats[$culture]->format($date, $format, null, $charset);
}

/**
 *
 */
function drupal_format_datetime($date, $format = 'F', $culture = null, $charset = null) {
  return drupal_format_date($date, $format, $culture, $charset);
}
