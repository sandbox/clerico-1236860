<?php
/*
 * This file is part of the Drupal Symfony module.
 * (c) 2010 J�r�me CLERICO <jerome.clerico@indigen.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 */
function symfony_admin_settings() {
  $form['symfony_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Symfony installation path'),
    '#default_value' => variable_get('symfony_path', ''),
    '#description' => t('Give your symfony installation filesystem path.'),
  );
  $form['symfony_application'] = array(
    '#type' => 'textfield',
    '#title' => t('Application'),
    '#default_value' => variable_get('symfony_application', 'frontend'),
    '#description' => t('Give your bound symfony application to handle interactions with your Drupal site.'),
  );
  $form['symfony_environment'] = array(
    '#type' => 'textfield',
    '#title' => t('Environment'),
    '#default_value' => variable_get('symfony_environment', 'dev'),
    '#description' => t('Give executing environment for your bound symfony application (ie. dev, test or prod)'),
  );
  $form['symfony_baseurl'] = array(
    '#type' => 'textfield',
    '#title' => t('Symfony base URL'),
    '#default_value' => variable_get('symfony_baseurl', ''),
    '#description' => t('Base URL for internal links (leave blank for default behaviour'),
  );

  return system_settings_form($form);
}

/**
 *
 */
function symfony_admin_flushcache() {
  drupal_flush_all_caches();
  drupal_set_message('Cache cleared.');
  drupal_goto($_SERVER['HTTP_REFERER']);
}

/* Pages management. */

/**
 *
 */
function symfony_admin_pages() {
  $mappings = variable_get('symfony_page_mappings', array());
 
  $out = "
<table>
  <tr>
    <th>".t("Title")."</th>
    <th>".t("Key")."</th>
    <th>".t("Path")."</th>
    <th>".t("Enabled")."</th>     
    <th colspan=\"2\" align=\"center\">".t("Control")."</th>                      
  </tr>
  ";
  
  foreach ( $mappings as $mapping ) {
    $options = array('attributes' => array('target'=>'_blank'));
    $url = l($mapping->path, $mapping->path, $options);
  
    $out .= "<tr>";
    $out .= "<td>".$mapping->title."</td>";
    $out .= "<td>".$mapping->key."</td>"; 
    $out .= "<td>". $url ."</td>"; 
    $enabled = ( $mapping->enabled ) ? t("Yes") : t("No");
    $out .= "<td>".$enabled."</td>"; 
    $out .= "<td>".l(t('Edit'), 'admin/build/symfony/edit/'.$mapping->key )."</td>"; 
    $out .= "<td>".l(t('Delete'), 'admin/build/symfony/delete/'.$mapping->key)."</td>";         
    $out .= "</tr>";    
  }
  
  $out .="</table>";
  
  $out .= _symfony_admin_clearCacheMessage();

  return $out;
}

/**
 *
 */
function symfony_admin_page_form(&$form_state, $saved = null ) {
  $saved = ( is_null($saved) ) ? new StdClass() : $saved;

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $saved->title,
    '#size' => 50,
    '#maxlength' => 50,
    '#description' => t('Title of the Symfony page.')
  );
 
  $form['key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#default_value' => $saved->key,
    '#required' => TRUE,
    '#size' => 50,
    '#maxlength' => 50,
    '#description' => t('Please indicate a unique key for this page. Template file and theme functions used
    for rendering will be named according to this key.<p><b>ATTENTION:</b> Do NOT use dash "-" in key names!</p>')
  );

  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('URI Symfony path (in the routing config file of your bound application)'),
    '#default_value' => $saved->path,
    '#required' => FALSE,    
    '#size' => 50,
    '#maxlength' => 50,
    '#description' => t('A Drupal URI path (relative to the root URI of Drupal) that this page should
    show up at. N/A for blocks.')
  );
  
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => (isset($saved->enabled))?$saved->enabled:True,
    '#description' => t('You can disable a page during development. Only users with \'administer symfony\' permission can access the page when it is disabled.')
  );
 
  if ( trim($saved->key) != "" ) {
    drupal_set_title(t('Edit Symfony page: %title', array('%title' => $saved->title)));
    $form['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#submit' => array('symfony_admin_page_form_submit')
    );
  } else {
    drupal_set_title(t('Add a Symfony page'));
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add Component'),
      '#submit' => array('symfony_admin_page_form_submit'),
    );
  }

  return $form;
}

/**
 *
 */
function symfony_admin_page_form_submit($form, &$form_state ) {
  $post = $form_state['values'];
  $mapping = new StdClass();
  $mapping->title = $post['title'];
  $mapping->key = $post['key'];  
  $mapping->path = $post['path'];    
  $mapping->enabled = $post['enabled'];    
  $mappings = variable_get('symfony_page_mappings', array());
  $mappings[$mapping->key] = $mapping;
  variable_set('symfony_page_mappings', $mappings);  
  drupal_flush_all_caches();
  drupal_goto('admin/build/symfony');
}

/**
 *
 */
function symfony_admin_delete_form(&$form_state, $mapping) {
  $form['title'] = array('#type' => 'hidden', '#value' => $mapping->title);
  $form['key'] = array('#type' => 'hidden', '#value' => $mapping->key);

  return confirm_form(
    $form,
    t('Delete Symfony page %title', array('%title' => $mapping->title)),
    'admin/build/symfony',
    '<p>'. t('Are you sure you want to delete the Symfony page %title?', array('%title' => $mapping->title)) .'</p>',
    t('Delete'),
    t('Cancel')
  );
}

/**
 *
 */
function symfony_admin_delete_form_submit($form, &$form_state) {
  $key = $form_state['values']['key'];
  $mappings = variable_get('symfony_page_mappings', null );
  if ( is_array( $mappings ) && is_object( $mappings[$key] ) )
    $new_settings = _symfony_admin_array_diff_key($mappings, array( $key => 'remove') );
  variable_set('symfony_page_mappings', $new_settings);
  drupal_flush_all_caches();
  drupal_set_message(t('The Symfony page: %title has been deleted.', array('%title' => $form_state['values']['title'])));
  $form_state['redirect'] = 'admin/build/symfony';
  return;
}

/* Block management. */

/**
 *
 */
function symfony_admin_block_list() {
  $blocks = variable_get('symfony_blocks', array());
  $_blocks = array();
  foreach ( $blocks as $delta => $block ) {
    if ( $block->admin_title )
      $_blocks[$delta]['info'] = $block->admin_title;
    else
      $_blocks[$delta]['info'] = t('Symfony block');
    $_blocks[$delta]['cache'] = BLOCK_NO_CACHE;
  }
  return $_blocks;
}

/**
 *
 */
function symfony_admin_block_configure_form($delta) {
  $blocks = variable_get('symfony_blocks', array());
  $block = ( ( isset($blocks[$delta]) ) ? $blocks[$delta] : new StdClass() );
  $form['admin_title'] = array(
    '#type' => 'textfield',
    '#default_value' => $block->admin_title,
    '#title' => t('Administrative title'),
    '#description' => t('This title will be used administratively to identify this block. If blank, the regular title will be used.'),
  );
  $form['slot_name'] = array(
    '#type' => 'textfield',
    '#default_value' => $block->slot_name,
    '#title' => t('Slot name'),
    '#description' => t('Identify a slot which can be use directly from Symfony'),
  );
  return $form;
}

/**
 *
 */
function symfony_admin_block_save($delta, $edit) {
  $block = new StdClass();
  $block->admin_title = $edit['admin_title'];
  $block->slot_name = $edit['slot_name'];
  $blocks = variable_get('symfony_blocks', array());
  $blocks[$delta] = $block;
  variable_set('symfony_blocks', $blocks);
}

/**
 *
 */
function symfony_admin_add_block_form(&$form_state) {
  module_load_include('inc', 'block', 'block.admin');
  return block_admin_configure($form_state, 'symfony', NULL);
}

/**
 *
 */
function symfony_admin_add_block_form_submit($form, &$form_state) {
  $blocks = variable_get('symfony_blocks', array());
  $delta = ( empty($blocks) ? 1 : max(array_keys($blocks)) + 1 );

  $block = new StdClass();
  $block->admin_title = $form_state['values']['admin_title'];
  $block->slot_name = $form_state['values']['slot_name'];
  $blocks[$delta] = $block;

  variable_set('symfony_blocks', $blocks);
  
  // Run the normal new block submission (borrowed from block_add_block_form_submit).
  foreach ( list_themes() as $key => $theme ) {
    if ( $theme->status )
      db_query("INSERT INTO {blocks} (visibility, pages, custom, title, module, theme, status, weight, delta, cache) VALUES(%d, '%s', %d, '%s', '%s', '%s', %d, %d, %d, %d)",
               $form_state['values']['visibility'], trim($form_state['values']['pages']), $form_state['values']['custom'], $form_state['values']['title'], $form_state['values']['module'], $theme->name, 0, 0, $delta, BLOCK_NO_CACHE);
  }

  foreach ( array_filter($form_state['values']['roles']) as $rid )
    db_query("INSERT INTO {blocks_roles} (rid, module, delta) VALUES (%d, '%s', '%s')",
             $rid, $form_state['values']['module'], $delta);
  
  drupal_set_message(t('The block has been created.'));
  cache_clear_all();

  $form_state['redirect'] = 'admin/build/block';
}

/**
 *
 */
function symfony_admin_block_form_alter(&$form, $form_state) {
  $blocks = variable_get('symfony_blocks', array());
  foreach ( array_keys($blocks) as $delta )
    $form['symfony_' . $delta]['delete'] = array('#value' => l(t('delete'), 'admin/build/block/delete-symfony-block/'. $delta));
}

/**
 * 
 */
function symfony_admin_block_delete(&$form_state, $delta = 0) {
  $blocks = variable_get('symfony_blocks', array());
  $title = ( ( isset($blocks[$delta]->admin_title) ) ? $blocks[$delta]->admin_title : 'Symfony block' );
  $form['block_title'] = array('#type' => 'hidden', '#value' => $title);
  $form['delta'] = array('#type' => 'hidden', '#value' => $delta);

  return confirm_form($form, t('Are you sure you want to delete the "%name" block?', array('%name' => $title)), 'admin/build/block', NULL, t('Delete'), t('Cancel'));
}

/**
 *
 */
function symfony_admin_block_delete_submit($form, &$form_state) {
  $delta = $form_state['values']['delta'];
  $blocks = variable_get('symfony_blocks', array());
  unset($blocks[$delta]);
  variable_set('symfony_blocks', array());
  db_query("DELETE FROM {blocks} WHERE module = 'symfony' AND delta = %d", $delta);
  db_query("DELETE FROM {blocks_roles} WHERE module = 'symfony' AND delta = %d", $delta);
  drupal_set_message(t('The "%name" block has been removed.', array('%name' => $form_state['values']['block_title'])));
  cache_clear_all();
  $form_state['redirect'] = 'admin/build/block';
  return;
}

/* Implementation. */

function _symfony_admin_clearCacheMessage() {
  return "<div style=\"border-top: 1px solid silver;\"><b>ATTENTION:</b> Theme engine is strongly cached in Drupal versions 6 and up and
  if/when you add a new tpl.php or a theming function, you need to ".l(t('clear cache'), 'admin/build/symfony/flushcache')." before you can see any changes.</div>";
}

function _symfony_admin_array_diff_key($arr1, $arr2) {
  if ( function_exists( 'array_diff_key' ) ) {
    return array_diff_key ( $arr1, $arr2 );
  } else {
    $arr3 = array();
    if ( ! is_array($arr1) || ! ( sizeof( $arr1 ) > 0 ) )
      return $arr3;
    foreach ( $arr1 as $key => $val ) {
      if ( ! array_key_exists($key, $arr2) )
        $arr3[$key] = $val;
    }
    return $arr3;
  }
}
