<?php
/*
 * This file is part of the Drupal Symfony module.
 * (c) 2010 J�r�me CLERICO <jerome.clerico@indigen.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 */
function symfony_page($key) {
  $response = symfony_getResponse(true);
  
  $contentType = $response->getContentType();
  if ( preg_match('/^text\/html/i', $contentType, $matches) ) {
    $text = $response->getContent(); 
    $text = preg_replace_callback('/{(symfony\:\/\/[^}]+)}/', 'symfony_filterCallback', $text);
    return $text;
  }

  // TODO: do a better thing
  ob_end_clean();
  header('Content-type: ' . $contentType);
  exit($response->getContent());
}

/**
 *
 */
function symfony_getPageMappings() {
  static $mappings = null;
  
  if ( $mappings !== null )
    return $mappings;
  
  $mappings = variable_get('symfony_page_mappings', array());
    
  return $mappings;
}

/**
 *
 */
function symfony_getBlocks() {
  static $blocks = null;
  
  if ( $blocs !== null )
    return $blocks;
  
  $blocks = variable_get('symfony_blocks', array());
  
  return $blocks;
}

/**
 *
 */
function symfony_getContext($create = true) {
  global $language; // TODO: better integration
  static $context = null;
  
  if ( !$create )
    return $context;

  if ( $context !== null )
    return $context;

  $path = variable_get('symfony_path', '');  // TODO: check path
  $application = variable_get('symfony_application', 'internet');
  $environment = variable_get('symfony_environment', 'dev');

  $filename = $path . '/config/ProjectConfiguration.class.php';
  
  if ( file_exists($filename) ) {
    require_once $filename; 
    $configuration = ProjectConfiguration::getApplicationConfiguration($application, $environment, true);
    $configuration->loadHelpers(array('Partial', 'I18N'));
    $context = DrupalContext::createInstance($configuration);
    $context->getUser()->setCulture($language->language); // TODO: better integration
    // TODO: integrates current drupal user
  }

  return $context;
}

/**
 *
 */
function symfony_getResponse($create = false) {
  static $response = null;
  if ( ($response === null) && $create ) {
    $context = symfony_getContext();
    $context->dispatch();
    $response = $context->getResponse();  
  }
  return $response;
}

/**
 *
 */
function symfony_filterCallback($matches) {
  $url = html_entity_decode($matches[1]);
  $parsing = parse_url($url);
  if ( isset($parsing['scheme']) && ($parsing['scheme'] == 'symfony') && isset($parsing['host']) && isset($parsing['path']) ) {
    if ( isset($parsing['query']) )
      parse_str($parsing['query'], $params);
    else
      $params = array();
    $context = symfony_getContext();
    if ( $context ) {
      try {
        return '<div class="symfony">' . get_component($parsing['host'], trim($parsing['path'], '/'), $params) . '</div>';
      } catch ( Exception $e ) {
        return '<font style="color:red">Symfony error: ' . $e->getMessage() . '</font>';
      }
    }
  }
  return $matches[0];
}

/* For debug. */

function symfony_trace($mixed = null, $message = '', $ip = '', $function = 'print_r') {
  if ( $ip && ($ip != $_SERVER['REMOTE_ADDR']) )
    return;
  ob_start();
  $trace = debug_backtrace();
  echo '<h1>Debug trace' . ( ( $message ) ? ' : ' . $message . "</h1>\n" : "</h1>\n" );
  echo '<b>File : </b>' . $trace[0]['file'] . "<br>\n";
  echo '<b>Line : </b>' . $trace[0]['line'] . "<br>\n";
  echo "<br>\n";
  echo "<pre>\n";
  call_user_func($function, $mixed);
  echo "</pre>\n";
  echo ob_get_clean();
  exit();
}

function symfony_stack() {
  $trace = debug_backtrace();
  $stack = array();
  foreach ( $trace as $item )
    $stack[] = $item['class'] . '::' . $item['function'] . '(' . $item['line'] . '@' . $item['file'] . ')';
  symfony_trace($stack);
}
